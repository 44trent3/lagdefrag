package com.minecraftonline.lagdefrag;

import com.google.inject.Inject;
import org.slf4j.Logger;
import org.spongepowered.api.event.game.state.GameStartedServerEvent;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.plugin.Plugin;

@Plugin(
        id = "lagdefrag",
        name = "LagDefrag",
        description = "The place where one should stick optimizations to Sponge.",
        authors = {
                "44trent3",
                "doublehelix457"
        }
)
public class LagDefrag {

    @Inject
    private Logger logger;

    @Listener
    public void onServerStart(GameStartedServerEvent event) {
    }
}
